<?php

    require_once ('animal.php');
    require_once ('frog.php');
    require_once ('ape.php');

    $shaun = new animal("Shaun");
    echo "nama : " . $shaun->nama ."<br>";
    echo "leg : " . $shaun->leg."<br>";
    echo "cold_blooded : " . $shaun->cold_blooded ."<br><br>";

    $kodok = new frog("buduk");
    echo "nama : " . $kodok->nama ."<br>";
    echo "leg : " . $kodok->leg ."<br>";
    echo "cold_blooded : " . $kodok->cold_blooded ."<br>";
    echo $kodok->jump();
    
        echo"<br><br>";

    $kera = new ape("kera");
    echo "nama : " . $kera->nama ."<br>";
    echo "leg : " . $kera->leg ."<br>";
    echo "cold_blooded : " . $kera->cold_blooded ."<br>";
    echo $kera->yell();

?>

